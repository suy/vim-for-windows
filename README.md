Vim for Windows
===============

--------------------------------------------------------------------------------

![Vim][Images/Vim]
![for][Images/for]
![Windows][Images/Windows]

Looking for Emacs?
==================

--------------------------------------------------------------------------------

If you are looking for bleeding-edge [Emacs][Emacs/Wikipedia] with decent set of
features for [Microsoft Windows][Windows/Wikipedia], then you could look into my 
[Emacs for Windows][].

Table of Contents
=================

--------------------------------------------------------------------------------

- [About](#markdown-header-about)
- [Installation](#markdown-header-installation)
    - [Requirements](#markdown-header-requirements)
        - [Platforms](#markdown-header-platforms)
        - [Architectures](#markdown-header-architectures)
    - [Instructions](#markdown-header-instructions)
        - [Scripting Interfaces](#markdown-header-scripting-interfaces)
            - [Python](#markdown-header-python)
            - [Ruby](#markdown-header-ruby)
            - [Lua](#markdown-header-lua)
            - [Perl](#markdown-header-perl)
- [Miscellaneous](#markdown-header-miscellaneous)
- [Downloads](#markdown-header-downloads)
    - [Featured](#markdown-header-featured)
    - [Previous](#markdown-header-previous)

About
=====

--------------------------------------------------------------------------------

Building [Vim][Vim/Wikipedia] (*Vi IMproved*) with decent set of features for
[Microsoft Windows][Windows/Wikipedia] is quite tricky and frustrating because
its build system is just insane piece of crap.

There are several projects which tried or are trying to provide Vim builds for
Windows. However, some of them are either already dead or struggle with crashes
from time to time, for example, because of using miserable toolchains (like
[Visual C++][Visual C++/Wikipedia]) and/or lack of experience in building native
software in general. I'm not going to poke a finger in these projects or provide
any links to them as this is irrelevant for further discussion after all.

The goal of this project is to provide high-quality native builds of Vim for
Windows with support for both x86 (32-bit) and x64 (64-bit) architectures,
"huge" feature set, and scripting interfaces for most popular programming
languages:

- [Python][Python/Wikipedia];
- [Ruby][Ruby/Wikipedia];
- [Lua][Lua/Wikipedia];
- [Perl][Perl/Wikipedia].

The latest source code used for builds is obtained directly from [official Vim
repository][Vim/Repository] maintained by its author, [Bram Moolenaar][Bram
Moolenaar/Wikipedia].

Installation
============

--------------------------------------------------------------------------------

Requirements
------------

### Platforms

- Windows 2000;
- Windows XP;
- Windows Vista;
- Windows 7;
- Windows 8.

### Architectures

- x86 (x86-32, x32, i686);
- x64 (x86-64, amd64).

Instructions
------------

1. Go to [Downloads](#markdown-header-downloads) and obtain archive with desired
   version and compatible with your current platform and architecture (see
   [Requirements](#markdown-header-requirements));
2. Extract the archive wherever you like;
3. Enjoy, and happy Vimming! `;)`

### Scripting Interfaces

I'm going to talk about trivial stuff here, but last time some people had issues
even with this part. Can you believe that?! `o_O`

First of all, please, understand that usually scripting interfaces come in the
form of *shared libraries* — external reusable binary components, intended to be
(*dynamically*, i.e. at *run time*) loaded and bound to some (already) running
process which depends on certain functionality provided by them. On Windows
platforms the concept of shared library is implemented as *dynamic-link library*
(DLL), while on Unix platforms it is implemented as *shared object* (SO).

Scripting interfaces for Vim are not an exception here. As a result, for them to
work properly in Vim, all you have to do is to make sure that their
corresponding DLLs (with versions that your chosen Vim distribution expects)
are *visible* to both `vim.exe` and `gvim.exe` (so that these DLLs can be loaded
and bound by Vim at run time). You have 2 options to achieve that:

1. Drop corresponding DLLs into the extracted Vim distribution, i.e. the same
   directory as `vim.exe` and `gvim.exe`;
2. Add paths to directories containing corresponding DLLs to the `PATH`
   environment variable.

Of course in overwhelming majority of cases you should choose option 2 because
that's how computer-literate people customize their environment, right? But if
you are still at school and/or don't know what `PATH` is, then I guess option 1
is fine, keep trying tho.

The next important thing to realize is that the targeted architecture of
corresponding DLLs has to match the targeted architecture of the Vim
distribution you chosen. In other words, if you have chosen 64-bit Vim
distribution, then you should expose 64-bit corresponding DLLs to it.
Conversely, if you have chosen 32-bit Vim distribution, then you should expose
32-bit corresponding DLLs to it. Why? Because *memory addressing models* (which
are defined by targeted architectures) of statically/dynamically linked binary
components must match, otherwise there is *binary incompatibility* and BOOM! The
universe folds back on itself... Don't do this at home... No, seriously, you
were warned!

Finally, all programming languages for the supported (by these Vim
distributions) scripting interfaces employ natural and widely accepted *triad
versioning scheme*:

    :::text
    <version> = <major>.<minor>.<patch>

Changes in the interface can only occur when the `<major>` and/or the `<minor>`
numbers are incremented, i.e. *backward compatibility* is broken, while
increments in the `<patch>` number preserve backward compatibility. What this
means for you is that you should not care about what is the `<patch>` number of
corresponding DLLs you expose to Vim. For instance, you can freely
(independently of your current Vim distribution) upgrade/downgrade corresponding
DLLs as long as changes occur only in the `<patch>` number, while the `<major>`
and the `<minor>` numbers remain complying with the ones your current Vim
distribution expects. Also, if you recall how, for example, Python (Lua, Perl)
DLLs are named, i.e. `python<major><minor>.dll`, e.g. `python34.dll`, then you'd
see that all the aforementioned suddenly makes sense again.

> **NOTE:** The only exception here is Ruby, which corresponding DLLs always
> contain the `<patch>` number in their names as well (see
> [Ruby](#markdown-header-ruby)).

Although the `<major>` and the `<minor>` (and the `<patch>` in case of Ruby
only) numbers of required versions for corresponding DLLs are listed in
[Downloads](#markdown-header-downloads) and appear in names of archives, you can
still find out which versions are required by your current Vim distribution from
either `vim.exe` or `gvim.exe` directly by simply typing `:version`. Look to the
bottom of the received message and you should see something similar to:

    :::text
    Dependency: python27.dll, python34.dll, x64-msvcrt-ruby200.dll, lua52.dll, perl518.dll, libintl.dll, libiconv.dll, iconv.dll

Now this tells you exactly which versions of corresponding DLLs your current Vim
distribution expects. Pay attention to the memory addressing model as well, for
example:

    :::text
    MS-Windows 64-bit GUI version with OLE support

reminds you that your current Vim distribution is targeted at x64 architecture,
and you have to expose corresponding DLLs targeting the same x64 architecture
accordingly.

#### Python

Not to mess things up, I highly recommend that you simply download [official
Windows MSI installers][Python/Downloads] which are relevant to your chosen Vim
distribution (see [Scripting
Interfaces](#markdown-header-scripting-interfaces)).

> **NOTE:** These Windows MSI installers will put Python DLLs into system
> directories:
>
> - `%SystemRoot%\System32`;
> - `%SystemRoot%\SysWOW64`;
>
> which are included into the `PATH` environment variable by default. Therefore,
> you don't have to do any additional manipulations to expose Python DLLs to
> your Vim distribution because everything should already work out of the box.

To test whether Python 2 works properly with your current Vim distribution,
type:

    :::text
    :py import sys; print(sys.version)

To test whether Python 3 works properly with your current Vim distribution,
type:

    :::text
    :py3 import sys; print(sys.version)

#### Ruby

Obtain [installers or archives][Ruby/Downloads] which are relevant to your
chosen Vim distribution (see [Scripting
Interfaces](#markdown-header-scripting-interfaces)).

> **NOTE:** Pay attention to the fact that for the Ruby scripting interface the
> `<patch>` number of the corresponding DLLs matters and has to match your
> current Vim distribution expectations (see
> [Downloads](#markdown-header-downloads)).

To test whether Ruby works properly with your current Vim distribution, type:

    :::text
    :ruby print RUBY_VERSION

#### Lua

Obtain [Windows DLL and includes (MinGW 4 compatible)][Lua/Downloads] which are
relevant to your chosen Vim distribution (see [Scripting
Interfaces](#markdown-header-scripting-interfaces)).

To test whether Lua works properly with your current Vim distribution, type:

    :::text
    :lua print(_VERSION)

#### Perl

Obtain [Windows MSI installers][Perl/Downloads] which are relevant to your
chosen Vim distribution (see [Scripting
Interfaces](#markdown-header-scripting-interfaces)).

To test whether Perl works properly with your current Vim distribution, type:

    :::text
    :perl VIM::Msg($^V)

Miscellaneous
=============

--------------------------------------------------------------------------------

If you are interested which toolchain I use to build [Vim for Windows][] as well
as other native software for Windows in general, then I'd be glad to say that
I'm using [MinGW-w64][] (not [MinGW][]!) which is a production quality
toolchain, bringing bleeding-edge [GCC][GCC/Wikipedia] features to Windows for
both x86 and x64 architectures.

Downloads
=========

--------------------------------------------------------------------------------

Featured
--------

| x64                                                                  | x86                                                                  | Vim     | Python 2 | Python 3 | Ruby  | Lua   | Perl   |
|:--------------------------------------------------------------------:|:--------------------------------------------------------------------:|:-------:|:--------:|:--------:|:-----:|:-----:|:------:|
| ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.417/x64] | ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.417/x86] | 7.4.417 | 2.7.X    | 3.4.X    | 2.0.0 | 5.2.X | 5.18.X |

Previous
--------

| x64                                                                  | x86                                                                  | Vim     | Python 2 | Python 3 | Ruby  | Lua   | Perl   |
|:--------------------------------------------------------------------:|:--------------------------------------------------------------------:|:-------:|:--------:|:--------:|:-----:|:-----:|:------:|
| ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.193/x64] | ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.193/x86] | 7.4.193 | 2.7.X    | 3.3.X    | 2.0.0 | 5.2.X | N/A    |
| ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.094/x64] | ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.094/x86] | 7.4.094 | 2.7.X    | 3.3.X    | 2.0.0 | N/A   | N/A    |
| ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.020/x64] | ![Download][Images/Download]* *[Download][Downloads/Vim/7.4.020/x86] | 7.4.020 | 2.7.X    | 3.3.X    | N/A   | N/A   | N/A    |

[Vim/Wikipedia]:  http://en.wikipedia.org/wiki/Vim_(text_editor)
[Vim/Repository]: http://code.google.com/p/vim/

[Python/Wikipedia]: http://en.wikipedia.org/wiki/Python_(programming_language)
[Python/Downloads]: http://www.python.org/download/

[Ruby/Wikipedia]: http://en.wikipedia.org/wiki/Ruby_(programming_language)
[Ruby/Downloads]: http://rubyinstaller.org/downloads/

[Lua/Wikipedia]: http://en.wikipedia.org/wiki/Lua_(programming_language)
[Lua/Downloads]: http://luabinaries.sourceforge.net/download.html

[Perl/Wikipedia]: http://en.wikipedia.org/wiki/Perl
[Perl/Downloads]: http://www.activestate.com/activeperl/downloads

[Windows/Wikipedia]: http://en.wikipedia.org/wiki/Microsoft_Windows

[Bram Moolenaar/Wikipedia]: http://en.wikipedia.org/wiki/Bram_Moolenaar

[GCC/Wikipedia]: http://en.wikipedia.org/wiki/GNU_Compiler_Collection

[Visual C++/Wikipedia]: http://en.wikipedia.org/wiki/Visual_C%2B%2B

[Emacs/Wikipedia]: http://en.wikipedia.org/wiki/Emacs

[MinGW]: http://www.mingw.org/

[MinGW-w64]: http://mingw-w64.sourceforge.net/

[Vim for Windows]:   /Haroogan/vim-for-windows
[Emacs for Windows]: /Haroogan/emacs-for-windows

[Downloads/Vim/7.4.417/x64]: /Haroogan/vim-for-windows/downloads/vim-7.4.417-python-2.7-python-3.4-ruby-2.0.0-lua-5.2-perl-5.18-windows-x64.zip
[Downloads/Vim/7.4.417/x86]: /Haroogan/vim-for-windows/downloads/vim-7.4.417-python-2.7-python-3.4-ruby-2.0.0-lua-5.2-perl-5.18-windows-x86.zip

[Downloads/Vim/7.4.193/x64]: /Haroogan/vim-for-windows/downloads/vim-7.4.193-python-2.7-python-3.3-ruby-2.0.0-lua-5.2-windows-x64.zip
[Downloads/Vim/7.4.193/x86]: /Haroogan/vim-for-windows/downloads/vim-7.4.193-python-2.7-python-3.3-ruby-2.0.0-lua-5.2-windows-x86.zip

[Downloads/Vim/7.4.094/x64]: /Haroogan/vim-for-windows/downloads/vim-7.4.094-python-2.7-python-3.3-ruby-2.0.0-windows-x64.zip
[Downloads/Vim/7.4.094/x86]: /Haroogan/vim-for-windows/downloads/vim-7.4.094-python-2.7-python-3.3-ruby-2.0.0-windows-x86.zip

[Downloads/Vim/7.4.020/x64]: /Haroogan/vim-for-windows/downloads/vim-7.4.020-python-2.7-python-3.3-windows-x64.zip
[Downloads/Vim/7.4.020/x86]: /Haroogan/vim-for-windows/downloads/vim-7.4.020-python-2.7-python-3.3-windows-x86.zip

[Images/Vim]:      https://bitbucket.org/Haroogan/vim-for-windows/raw/master/images/400-400/vim.png               "Vim"
[Images/for]:      https://bitbucket.org/Haroogan/vim-for-windows/raw/master/images/200-400/green-right-arrow.png "for"
[Images/Windows]:  https://bitbucket.org/Haroogan/vim-for-windows/raw/master/images/400-400/windows.png           "Windows"
[Images/Download]: https://bitbucket.org/Haroogan/vim-for-windows/raw/master/images/16-16/download.png            "Download"
